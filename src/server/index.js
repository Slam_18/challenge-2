const express = require('express');
const app = express();
const path = require('path');

app.set('view engine','ejs');
app.set('views', '/Users/kevin_herrera/Desktop/JS-EJEMPLO/src/views');

app.use(express.static('./service/logic.js'));

// asignando una variable y un valor a la app
app.set('port', 8080);

// route
app.use(require('../routes/routes'));

//estableciendo el puerto por el cual escuchara
app.listen(app.get('port'),()=>{
    console.log(__dirname);
});




